const merge = require('webpack-merge')
const common = require('./webpack.common')

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        // 允许微前端basis加载子应用
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        static: ['./dist', 'public'],
        historyApiFallback: true
    }
})
# webpack_demo

#### 项目介绍
自己基于webpack搭建的一个集成react, typescript, redux toolkit项目，用来练习一些插件的使用，或者对应不太确定的点实现一些简单的demo去验证

#### 软件架构
+ webpack版本升级(^4.41.2 -> 5.65.0)
extract-text-webpack-plugin -> mini-css-extract-plugin


+ 引入TypeScript
    * Dispatch类型定义在redux中
    ```javascript
    import { Dispatch } from 'redux'

    const mapDispatchToProps = (dispatch: Dispatch, ownProps: { filter: string }) => {
        return {
            onClick: () => {
                dispatch(setVisibilityFilter(ownProps.filter))
            }
        }
    }
    ```

    * Event类型参数
    ```javascript
    addTodo = () => {
        const todo = (document.querySelector('.todo-input') as HTMLInputElement).value
        this.props.dispatch(addTodo(todo));
        (document.querySelector('.todo-input') as HTMLInputElement).value = ''
    }

    onSubmit = (e: React.FormEvent) => {
        e.preventDefault()
        this.addTodo()
    }



    render() {
        return (
        <div>
            <form onSubmit={this.onSubmit}>
            <input className='todo-input' type="text"></input>
            <button type='button' onClick={this.addTodo}>Add</button>
            </form>
        </div>
        );
    }
    ```

    * Redux && TypeScript

    下面的说明来自[Usage with TypeScript](http://cn.redux.js.org/recipes/usage-with-typescript/)
    > As of React Redux v7.2.3, the react-redux package has a dependency on @types/react-redux, so the type definitions will be automatically installed with the library. Otherwise, you'll need to manually install them yourself (typically npm install @types/react-redux ).


    * 关于ts-loader
    
    开始使用的awesome-typescript-loader，npm start报下面的错误，改用ts-loader就可以正常启动了
    ```
    UnhandledPromiseRejection: This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). The promise rejected with the reason "null".
    ```

    * react-devtools使用探究

    * TypeScript + React 代码(手写拖拽)

    ```JAVASCRIPT
    // TypeScript引入类型要注意是从哪个包引入类型
    import React, { DragEvent } from 'react'

    export default function App() {
        const drag = (e: React.DragEvent<HTMLImageElement>) => {
            e.dataTransfer.setData("id", (e.target as HTMLImageElement).id)
        }

        const allowDrop = (e: React.DragEvent) => {
            e.preventDefault()
        }

        const drop = (e: React.DragEvent) => {
            e.preventDefault()
            const id = e.dataTransfer.getData("id");
            (e.target as HTMLDivElement).appendChild(document.getElementById(id))
        }
        return <div>
            <div className="container" onDragOver={allowDrop} onDrop={drop}></div>
            <img id='martin' src="images/martin.png" onDragStart={drag} draggable alt="" />
        </div>
    }
    ```

    * React import 图片 TypeScript
    
    [react: typescript import images alias](https://www.cnblogs.com/Nyan-Workflow-FC/p/11225231.html)

    * TypeScript集成React Router出现下面的问题是怎么回事？

    ```
    Module '"react-router-dom"' has no exported member 'Switch'.ts(2305)
    ```
    注意，react-router-dom v6没有Switch组件了，取而代之的是Routes组件


    * TypeScript自定义React组件添加className等属性报错，应该怎么解决？

    ```javascript
    interface MenuItemProps {
        tag: Menu,
        handleClickDelete?: (id: number) => void
        handleClickAdd?: (id: number) => void
    }

    function MenuItem(props: MenuItemProps) {
        const {
            tag,
            handleClickDelete,
            handleClickAdd
        } = props
        return <div className="tag">
            <img draggable="false" className='tag-icon' src={tag.icon} alt="" />
            <span className='tag-title'>{tag.title}</span>
            {
                tag.selected ?
                    <div
                        className="btn-delete"
                        onClick={() => handleClickDelete(tag.id)}
                    /> :
                    <div
                        className="btn-add"
                        onClick={() => handleClickAdd(tag.id)}
                    />
            }
        </div>
    }

    <MenuItem
        key={index}
        tag={menu}
        handleClickAdd={handleClickAdd}
    ></MenuItem>
    ```

    ```
    Type '{ className: string; tag: any; handleClickDelete: (id: number) => void; }' is not assignable to type 'IntrinsicAttributes & MenuItemProps'.
    Property 'className' does not exist on type 'IntrinsicAttributes & MenuItemProps'.ts(2322)
    ```

    ```javascript
    interface MenuItemProps extends React.HTMLAttributes<HTMLElement>
    ```

    * React函数式组件如果声明为React.FC, 那么props类型应该用React.FC<PropsType>的方式声明

    ```javascript
    export const Conditional: React.FC<{ active: boolean, children: ReactNode }> = (props) => {
        // ...
    }

    // 如果像下面这样声明，会报错
    export const Conditional: React.FC = (props: { active: boolean, children: ReactNode }) => {
        // ...
    }
    ```

    ```javascript
    TS2322: Type '{ children: Element; active: boolean; }' is not assignable to type 'IntrinsicAttributes & { children?: ReactNode; }'.
    Property 'active' does not exist on type 'IntrinsicAttributes & { children?: ReactNode; }'.
    ```
    

    * React组件className不会像vue那样自动合并而是覆盖怎么解决？
    ```javascript
    const {
        tag,
        handleClickDelete,
        handleClickAdd,
        className
    } = props
    return <div className={`${className} tag`}>
    ```

    * 如果一个变量可能是无函数式组件也有可能是Class组件，那么其类型应该怎么声明？
    ```javascript
    export interface Pane {
        key: string,
        title: string,
        to: string,
        content: React.FC | React.ComponentClass
    }
    ```

    * 引入form-render示例报错
    
        解决步骤：

        1. 参考[webpack使用Less文件的配置](https://blog.csdn.net/jwz934738949/article/details/107457774)
        ```javascript
        // 导入less文件
      {
        test: /\.less$/,
        use: [{
          loader: "style-loader" // creates style nodes from JS strings
        }, {
          loader: "css-loader" // translates CSS into CommonJS
        }, {
          loader: "less-loader" // compiles Less to CSS
        }]
      }
        
        ```
        2. 还是报错，说是Cannot find module 'less'，参考[Error: Cannot find module 'less'](https://www.cnblogs.com/wayneliu007/p/10753772.html)
        3. 又报错
        ```javascript
        AntDesign .bezierEasingMixin()； ^ Inline JavaScript is not enabled. Is it set in your options?
        ```
        参考[AntDesign .bezierEasingMixin()； ^ Inline JavaScript is not enabled. Is it set in your options?](https://blog.csdn.net/wuyujin1997/article/details/111999322)
        添加了如下webpack配置
        ```javascript
        {
            test: /\.(css|less)$/,
            use: [
                { loader: "style-loader" },
                { loader: "css-loader" },
                {
                    loader: "less-loader",
                    options: {
                        lessOptions: {
                            javascriptEnabled: true,
                        },
                    },
                },
            ],
        },
        ```
        4. 还是报上面的错误，参考[bezierEasingMixin(); Inline JavaScript is not enabled. Is it set in your options?](https://blog.csdn.net/luckyone1111/article/details/85004217)，修改webpack配置如下
        ```javascript
        options: { javascriptEnabled: true }
        ```
        OK了

    * TypeScript怎么打印某种类型？
    ```javascript
    interface Animal {
        name: string
    }

    interface Dog extends Animal {
        bark(): void
    }
    // 'Dog' only refers to a type, but is being used as a value here.
    console.log(Dog)
    ```

    * React createRef & useRef ?

    * bpmn-js是否支持typescript，官方不提供类型声明文件 ?

    * 安装react-router-dom需要同时安装react-router吗？



        


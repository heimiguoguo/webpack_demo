// css模块声明
declare module '*.module.css' {
    const classes: { readonly [key: string]: string }
    export default classes
}

declare module "*.svg" {
    const content: any;
    export default content;
}

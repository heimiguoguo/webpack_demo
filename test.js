const moment = require('moment')
const { findIndex } = require('lodash')

// new Promise((resolve, reject) => {
//     reject("hello")
// }).then(
//     async res1 => {
//         console.log(res1)
//         new Promise((resolve, reject) => {
//             resolve("world")
//         }).then(async res2 => {
//             if (res2 === 'world') {
//                 const res3 = await new Promise((resolve, reject) => {
//                     resolve('fuck')
//                 })
//                 console.log(res3)
//             }
//         })
//     },
//     (err) => {
//         console.log(err)
//     }
// )

// (async () => {
//     try {
//         const result = await new Promise((resolve, reject) => {
//             // resolve('OK')
//             reject('ERROR')
//         })
//         console.log(result)
//     } catch (err) {
//         console.log('####', err)
//     }
// })()

// // 立即执行函数
// for (i = 0; i < 5; i++) {
//     ((i) => {
//         setTimeout(() => {
//             console.log(i)
//         }, 100)
//     })(i)
// }


// console.log(moment('2022-03-30').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log(moment('2022-03-29').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log(moment('2022-03-28').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log(moment('2022-03-27').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log('')

// console.log(moment('2020-03-30').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log(moment('2020-03-29').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log(moment('2020-03-28').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log(moment('2020-03-27').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log('')

// console.log(moment('2022-09-28').subtract(1, 'M').format('YYYY-MM-DD'))
// console.log(moment('2022-09-30').subtract(1, 'M').format('YYYY-MM-DD'))

// console.log(moment(undefined).format('YYYY_MM_DD'))
// console.log(moment(null).format('YYYY_MM_DD'))
// console.log(moment('2022-11-03').format('YYYY_MM_DD'))

const format = 'HH:mm:ss MM/D/YYYY'
const startTime = moment('16:24:33 04/28/20', format);
const endTime = moment('20:24:33 05/30/20', format);

const diff1 = endTime.diff(startTime, 'years')
const diff2 = endTime.diff(startTime, 'months', true)
const diff3 = endTime.diff(startTime, 'days')
const diff4 = endTime.diff(startTime, 'minutes')
const diff5 = endTime.diff(startTime, 'seconds')

// console.log(diff1, diff2, diff3, diff4, diff5)
// console.log(diff2)


console.log(moment('2022-09-28').format('YYYY-MM-DD HH:mm:ss'))
console.log(moment(1677210045971).format('YYYY-MM-DD HH:mm:ss'))
// Deprecation warning: value provided is not in a recognized RFC2822 or ISO format.
// console.log(moment('1677210045971').format('YYYY-MM-DD HH:mm:ss'))
console.log(typeof moment('2022-09-28').format('YYYY-MM-DD'))

// const arr = [4, 2]
// const result = findIndex([1, 2, 3], item => {
//     const temp = arr.includes(item)
//     console.log(item, temp)
//     return temp
// })

// console.log(result)




import React, { useState } from 'react';
import { DraggableArea } from 'react-draggable-tags';
// import deleteBtn from '/images/delete.png';
// import deleteBtn2x from '/images/delete@2x.png';
import './LiuXi.css';
import { menus as mock } from './mock';

interface Menu {
    id: number,
    title: string,
    icon: string,
    selected?: boolean
}

interface MenuItemProps extends React.HTMLAttributes<HTMLElement> {
    tag: Menu,
    handleClickDelete?: (id: number) => void
    handleClickAdd?: (id: number) => void
}

function MenuItem(props: MenuItemProps) {
    const {
        tag,
        handleClickDelete,
        handleClickAdd,
        className
    } = props
    return <div className={`${className} tag`}>
        <img draggable="false" className='tag-icon' src={tag.icon} alt="" />
        <span className='tag-title'>{tag.title}</span>
        {
            tag.selected ?
                <div
                    className="btn-delete"
                    onClick={() => handleClickDelete(tag.id)}
                /> :
                <div
                    className="btn-add"
                    onClick={() => handleClickAdd(tag.id)}
                />
        }
    </div>
}

export default function AddAndDelete() {

    const [menus, setMenus] = useState(mock)
    const [selectedMenus, setSelectedMenus] = useState([])

    const handleClickAdd = (id: number) => {
        setMenus(menus.filter(menu => menu.id !== id))
        setSelectedMenus([...selectedMenus, { ...mock.find(menu => menu.id === id), selected: true }])
    }

    const handleClickDelete = (id: number) => {
        setSelectedMenus(selectedMenus.filter(menu => menu.id !== id))
        setMenus([...menus, { ...mock.find(menu => menu.id === id), selected: false }])
    }

    const onChange = (tags: Menu[]) => {
        setSelectedMenus(tags)
    }

    return (
        <div className="xi-liu">
            <div className="main">
                <h3>我的应用</h3>
                <DraggableArea
                    tags={selectedMenus}
                    render={({ tag }) => (
                        <MenuItem
                            className={tag.id === 3 ? "diff" : ""}
                            tag={tag}
                            handleClickDelete={handleClickDelete}
                        ></MenuItem>
                    )}
                    onChange={onChange}
                />
            </div>
            <div className="main">
                <h3>更多应用</h3>
                <div className="menu-list">
                    {
                        menus.filter(menu => !menu.selected).map((menu, index) => (
                            <MenuItem
                                key={index}
                                tag={menu}
                                handleClickAdd={handleClickAdd}
                            ></MenuItem>
                        ))
                    }
                </div>
            </div>
        </div>
    );
}
import React, { Component } from 'react';
import { DraggableArea } from 'react-draggable-tags';
// import deleteBtn from '/images/delete.png';
// import deleteBtn2x from '/images/delete@2x.png';
import './style.module.css';
import mock from './mock';


interface AddAndDeleteProps {

}

interface Tag {
    id: string,
    content: string
}

interface AddAndDeleteState {
    tags: Tag[]
}

export default class AddAndDelete extends Component<AddAndDeleteProps, AddAndDeleteState> {
    private input: HTMLInputElement
    constructor(props: AddAndDeleteProps) {
        super(props);
        this.state = {
            tags: mock.tags
        };

        this.handleClickAdd = this.handleClickAdd.bind(this);
    }

    handleClickAdd() {
        if (this.input.value.length < 1) return;
        const tags = this.state.tags.slice();
        tags.push({ id: Math.random().toString(), content: this.input.value });
        this.setState({ tags });
        this.input.value = '';
    }

    handleClickDelete(tag: Tag) {
        const tags = this.state.tags.filter(t => tag.id !== t.id);
        this.setState({ tags });
    }

    render() {
        return (
            <div className="AddAndDelete">
                <div className="main">
                    <DraggableArea
                        tags={this.state.tags}
                        render={({ tag, index }) => (
                            <div className="tag">
                                <img
                                    className="delete"
                                    src="/images/delete.png"
                                    srcSet="/images/delete.png 2x"
                                    onClick={() => this.handleClickDelete(tag)}
                                />
                                {tag.content}
                            </div>
                        )}
                        onChange={tags => this.setState({ tags })}
                    />
                </div>
                <div className="inputs">
                    <input ref={r => this.input = r} />
                    <button onClick={this.handleClickAdd}>Add</button>
                </div>
            </div>
        );
    }
}
export default {
    tags: [
        {
            id: '1',
            content: 'apple',
        },
        {
            id: '2',
            content: 'watermelon',
        },
        {
            id: '3',
            content: 'banana',
        },
        {
            id: '4',
            content: 'lemon',
        },
        {
            id: '5',
            content: 'orange',
        },
        {
            id: '6',
            content: 'grape',
        },
        {
            id: '7',
            content: 'strawberry',
        },
        {
            id: '8',
            content: 'cherry',
        },
        {
            id: '9',
            content: 'peach',
        }],
}

export const menus = [
    {
        id: 1,
        title: '产品管理',
        icon: '/images/liuxi/产品管理.png'
    },
    {
        id: 2,
        title: '公司制度',
        icon: '/images/liuxi/公司制度.png'
    },
    {
        id: 3,
        title: '人才管理',
        icon: '/images/liuxi/人才管理.png'
    },
    {
        id: 4,
        title: '社区',
        icon: '/images/liuxi/社区.png'
    },
    {
        id: 5,
        title: '审批',
        icon: '/images/liuxi/审批.png'
    },
    {
        id: 6,
        title: '信息窗口',
        icon: '/images/liuxi/信息窗口.png',
        selected: false
    },
    {
        id: 7,
        title: '邮箱',
        icon: '/images/liuxi/邮箱.png'
    },
    {
        id: 8,
        title: '员工指南',
        icon: '/images/liuxi/员工指南.png'
    },
    {
        id: 9,
        title: '云财',
        icon: '/images/liuxi/云财.png'
    },
    {
        id: 10,
        title: '云采',
        icon: '/images/liuxi/云采.png'
    },
    {
        id: 11,
        title: '云旅',
        icon: '/images/liuxi/云旅.png'
    },
]
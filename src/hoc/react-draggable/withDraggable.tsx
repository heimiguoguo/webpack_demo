import React from 'react'
import Draggable from 'react-draggable'

function withDraggable(WrappedComponent: React.ComponentType) {
    return <Draggable>
        <div>
            <WrappedComponent></WrappedComponent>
        </div>
    </Draggable>
}

export default withDraggable
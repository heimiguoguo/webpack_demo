export default {
    address: "West Lake",
    name: 'Tom',
    user: {
        name: 'Jimmy Lin',
        address: 'YiDaYun Mountain Lake'
    },
    column: {
        name: 'Name',
        age: 'Age',
        address: 'Address'
    },
    placeholder: 'Please input'
}
import { flatten } from 'flat'
import en from './directives/en'
import zhCN from './directives/zh-CN'
import zhTW from './directives/zh-TW'

const flattedEn = flatten(en)
const flattedCN = flatten(zhCN)
const flattedTW = flatten(zhTW)

export default {
    'en-US': flattedEn,
    'zh-CN': flattedCN,
    'zh-TW': flattedTW
}
import React, { forwardRef, MutableRefObject, useEffect, useImperativeHandle, useRef } from 'react';

interface MergerProps extends React.HTMLAttributes<HTMLElement> {
    ref: MutableRefObject<any>
    preview: boolean
    mergedPdfUrl: string
}


// files: Array of PDF File or Blob objects
export const Merger = forwardRef(({ preview, mergedPdfUrl }: MergerProps, ref) => {
    const iframeRef = useRef(null)

    useImperativeHandle(ref, () => {
        return {
            print: () => {
                iframeRef.current.contentWindow.print()
            }
        }
    })

    return <iframe
        style={{ display: !preview ? 'none' : 'block' }}
        ref={iframeRef}
        height={1000}
        src={`${mergedPdfUrl}`}
        title='pdf-viewer'
        width='100%s'
    ></iframe>
})
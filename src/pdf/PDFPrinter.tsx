import React, { useEffect, useRef, useState } from 'react';
import axios from 'axios';
import PDFMerger from 'pdf-merger-js/browser';
import { Merger } from './Merger'
import { Button } from 'antd';

interface PDFPrinterProps {
    preview: boolean
    ids: number[]
}

export const PDFPrinter: React.FC<PDFPrinterProps> = ({ preview, ids }) => {
    const [files, setFiles] = useState([])
    const [mergedPdfUrl, setMergedPdfUrl] = useState('');
    const ref = useRef(null)

    const render = async (files: string[]) => {
        const merger = new PDFMerger();

        for (const file of files) {
            await merger.add(file)
        }

        const mergedPdf = await merger.saveAsBlob();
        const url = URL.createObjectURL(mergedPdf);

        return setMergedPdfUrl(url);
    };

    useEffect(() => {
        if (files.length) {
            render(files).catch((err) => {
                throw err;
            })
        }
        () => setMergedPdfUrl('')
    }, [files, setMergedPdfUrl])

    const getPdf = async () => {
        // const res = await axios.post('http://localhost:3001/api/pdf', ids)
        // setFiles(res.data)
        setFiles(['/mac java开发环境搭建.pdf'])
    }

    useEffect(() => {
        ids.length && getPdf()
    }, [ids])

    const print = () => {
        if (files.length) {
            ref.current.print()
        } else {
            getPdf()
        }
    }

    return <div>
        {
            !preview && <Button type='primary' onClick={print}>print</Button>

        }
        <Merger
            ref={ref}
            preview={preview}
            mergedPdfUrl={mergedPdfUrl}
        />
    </div>
}
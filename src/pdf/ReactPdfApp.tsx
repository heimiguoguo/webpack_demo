import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom'
import { Document, Page } from 'react-pdf/dist/esm/entry.webpack'
import { Button, Pagination, Radio, RadioChangeEvent } from 'antd'

interface PDFDocumentProps {
    mode: 'all' | 'page'
    numPages: number
    pageNumber: number
    onDocumentLoadSuccess: ({ numPages }: { numPages: number }) => void
}

const PDFDocument: React.FC<PDFDocumentProps> = ({ mode, numPages, pageNumber, onDocumentLoadSuccess }) => {


    const Content = () => {
        return mode === 'all' ? <>
            {
                Array.from({ length: numPages }).map((item, index) => {
                    return <Page key={index} pageNumber={index + 1} />
                })
            }
        </> : <Page pageNumber={pageNumber} />
    }

    return <>


        <div className="compare" style={{ display: 'flex' }}>
            <div className="react-pdf-container">
                <Document file="training.pdf" onLoadSuccess={onDocumentLoadSuccess}>
                    <Content />
                </Document>
            </div>
        </div>


    </>
}

function MyApp() {

    const [position, setPosition] = useState('parent window')

    const [mode, setMode] = useState<'all' | 'page'>('all')

    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);

    function onDocumentLoadSuccess({ numPages }: { numPages: number }) {
        setNumPages(numPages);
    }

    const onChange = (pageNumber: number) => {
        setPageNumber(pageNumber)
    }

    const onModeChange = (e: RadioChangeEvent) => {
        setMode(e.target.value)
    }

    const onPositionChange = (e: RadioChangeEvent) => {
        setPosition(e.target.value)
    }

    const attachPDF = () => {
        const target = position === 'parent window' ?
            document.querySelector('.pdf-container') :
            document.querySelector('iframe').contentWindow.document.querySelector('.pdf-container')
        ReactDOM.render(
            <PDFDocument
                mode={mode}
                numPages={numPages}
                pageNumber={pageNumber}
                onDocumentLoadSuccess={onDocumentLoadSuccess}
            />,
            target
        )
    }

    // useEffect(() => {
    //     attachPDF()
    // }, [])

    // useEffect(() => {
    //     attachPDF()
    // }, [mode])

    const print = () => {
        const target = position === 'iframe' ? document.querySelector('iframe').contentWindow : window
        target.print()
    }

    const srcDoc = "<div class='pdf-container' style='color:red'><img src='images/timg.jpg'></img></div>"

    return (
        <div className='react-pdf-app' style={{ height: '100%' }}>
            {/* <div className="operation-area">
                <div>
                    PDF Attach target position: <Radio.Group onChange={onPositionChange} value={position}>
                        <Radio value='parent window'>PDF container in parent window</Radio>
                        <Radio value='iframe'>PDF container in iframe</Radio>
                    </Radio.Group>
                </div>

                <div>
                    Display Mode: <Radio.Group onChange={onModeChange} value={mode}>
                        <Radio value='all'>All</Radio>
                        <Radio value='page'>Page</Radio>
                    </Radio.Group>
                </div>

                <Button onClick={attachPDF}>Attach pdf to iframe</Button>
                <Button onClick={print}>Print</Button>
            </div> */}

            {/* <PDFDocument
                mode={mode}
                numPages={numPages}
                pageNumber={pageNumber}
                onDocumentLoadSuccess={onDocumentLoadSuccess}
            /> */}

            {/* <iframe src="training.pdf" frameBorder="0" width={1000} height={1000}></iframe> */}
            {
                position === 'iframe' ?
                    <iframe
                        id='pdf-iframe'
                        width='1000'
                        height='1000'
                        // 这里需要实时将PDFDocument编译成DOM，但是ReactDOM.render是需要指定一个target的
                        // https://blog.csdn.net/weixin_44475093/article/details/119259484
                        // srcDoc={<PDFDocument />}
                        srcDoc={srcDoc}
                        frameBorder="0"
                    ></iframe> :
                    <div className="pdf-container"></div>
            }

            {
                mode === 'page' && <Pagination showQuickJumper pageSize={1} total={numPages} onChange={onChange} />
            }

        </div>
    );
}

export default MyApp
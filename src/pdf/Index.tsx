import React, { useState } from 'react'
import { PDFPrinter } from './PDFPrinter'
import CheckableTable, { DataType } from './CheckableTable'

export default () => {

    const [ids, setIds] = useState([])

    // rowSelection object indicates the need for row selection
    const rowSelection = {
        onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
            setIds(selectedRowKeys)
        },
        getCheckboxProps: (record: DataType) => ({
            disabled: record.name === 'Disabled User', // Column configuration not to be checked
            name: record.name,
        }),
    };
    return <div className="hello">
        <CheckableTable rowSelection={rowSelection} />
        <PDFPrinter preview={false} ids={ids} />
    </div>
}
import React, { ReactNode, useRef } from 'react'
import { Descriptions } from 'antd'

interface GrandsonProps {
    ref: React.Ref<ReactNode>
}

const Grandson: React.FC<GrandsonProps> = () => {
    const sayHello = () => {
        console.log('I am your grandson...')
    }
    return <div className="grandson">
        <h5>Grandson</h5>
    </div>
}

const Son = () => {
    // Function components cannot be given refs. Attempts to access this ref will fail. Did you mean to use React.forwardRef()?
    const grandsonRef = useRef()
    return <div className="son">
        <h4>Son</h4>
        <Grandson ref={grandsonRef} />
    </div>
}

interface DescriptionsItemProps {
    label: string
    value: string
}
const DescriptionsItem: React.FC<DescriptionsItemProps> = ({ label, value }) => {
    // 甚至what都不会打印
    console.log('what')
    return <Descriptions.Item label="UserName456">{value}</Descriptions.Item>
}
interface DescriptionsItemContentProps {
    children: React.ReactElement
}
const DescriptionsItemContent: React.FC<DescriptionsItemContentProps> = ({ children }) => {
    console.log('hello')
    return children
}

const Parent = () => {

    const list = [
        {
            label: 'Remark',
            value: 'Empty'
        },
        {
            label: 'Address',
            value: 'No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China'
        },
    ]
    return <Descriptions title="User Info">
        {/* 为什么自定义组件不能正常显示value */}
        <DescriptionsItem label="UserName123" value="Zhou Maomao" />
        <Descriptions.Item label="UserName">Zhou Maomao</Descriptions.Item>
        <Descriptions.Item label="Telephone">1810000000</Descriptions.Item>
        <Descriptions.Item label="Telephone">
            <DescriptionsItemContent>
                <div className="hello">15671665887</div>
            </DescriptionsItemContent>
        </Descriptions.Item>
        <Descriptions.Item label="Live">Hangzhou, Zhejiang</Descriptions.Item>
        <Descriptions.Item label="Remark">empty</Descriptions.Item>
        <Descriptions.Item label="Address">
            No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China
        </Descriptions.Item>
        {
            list.map(({ label, value }, index) => {
                return <Descriptions.Item key={index + 100} label={label}>{value}</Descriptions.Item>
            })
        }
    </Descriptions>
}

export default Parent
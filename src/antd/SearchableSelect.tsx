import React, { useState } from 'react'
import { Select } from 'antd';

const { Option } = Select;

function onChange(value: string) {
    console.log(`selected ${value}`);
}

function onSearch(val: string) {
    console.log('search:', val);
}

const SearchableSelect = () => {
    return <>
        <Select
            placeholder="Select a person"
            // if mode is 'multiple' or 'tags', showSearch is set to be true by default
            mode='multiple'
            // when mode is 'multiple' or 'tags', showSearch can be set to be false, it works
            showSearch={false}
            // optionFilterProp="children"
            onChange={onChange}
            style={{ width: 200 }}
        // onSearch={onSearch}
        // filterOption={(input, option: { children: string }) => {
        //     // console.log(option)
        //     return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
        // }}
        // filterOption={true}
        >
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="tom">Tom</Option>
        </Select>
    </>
}

export default SearchableSelect
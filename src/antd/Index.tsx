import React from 'react'
import { Layout, Menu } from 'antd'
import { Link, Outlet } from 'react-router-dom';
import { routes } from '../router'

const { Sider, Content } = Layout;


export default () => {
    return <div className="antd-demo" style={{ height: '100%' }}>
        <Layout style={{ height: '100%' }}>
            <Sider>
                <Menu
                    style={{ width: 200, height: '100%' }}
                    // selectedKeys={[activeKey]}
                    mode="inline"
                    items={routes[4].routes.map((route) => {
                        return {
                            key: route.path,
                            label: <Link to={route.path}>{route.name}</Link>
                        }
                    })}
                />
            </Sider>
            <Content style={{ margin: '0 20px' }}>
                {/* 这里一定要放一个<Outlet>，它就像vue router里边的 router-view */}
                <Outlet></Outlet>
            </Content>
        </Layout>
    </div>
}
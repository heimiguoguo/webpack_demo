import React, { useState, useRef, useEffect, useContext } from 'react'
import { Input, Form, type InputRef } from 'antd';
import EditableContext from './EditableContext'


interface Item {
    key: string;
    name: string;
    age: string;
    address: string;
}

interface EditableCellProps {
    title: React.ReactNode;
    children: React.ReactNode;
    dataIndex: keyof Item;
    record: Item;
    FormItem?: React.FC<{ disabled: boolean, save: () => void }>
    handleSave: (record: Item) => void;
}

const EditableCell: React.FC<EditableCellProps> = ({
    title,
    children,
    dataIndex,
    record,
    FormItem,
    handleSave,
    ...restProps
}) => {
    const form = useContext(EditableContext)!;

    useEffect(() => {
        if (dataIndex && record && record[dataIndex] !== undefined) {
            form.setFieldsValue({ [dataIndex]: record[dataIndex] })
        }
    }, []);


    const save = async () => {
        try {
            const values = await form.validateFields();
            handleSave({ ...record, ...values });
        } catch (errInfo) {
            console.log('Save failed:', errInfo);
        }
    };

    let childNode = children;

    childNode = FormItem ? (
        <FormItem
            disabled={false}
            save={save}
        />
    ) : (
        <div className="editable-cell-value-wrap" style={{ paddingRight: 24 }}>
            {children}
        </div>
    );

    return <td {...restProps}>{childNode}</td>;
};

export default EditableCell
import React from 'react'
import type { FormInstance } from 'antd/lib/form';


const EditableContext = React.createContext<FormInstance<any> | null>(null);
const FormListContext = React.createContext<(any)>({});
export default EditableContext
export { FormListContext }
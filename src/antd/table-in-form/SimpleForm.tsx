import React, { useRef } from 'react'
import { Form, Input, Button, Checkbox } from 'antd';
import EditableTable from './EditableTable'


const Demo = () => {

    const friendsRef = useRef<any>(null)

    const onFinish = (values: any) => {
        friendsRef.current.validateFormList()
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 6 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
        >
            <Form.Item
                label="Username"
                name="username"
                rules={[{ required: true, message: 'Please input your username!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item label="紧急联系人" wrapperCol={{ span: 20 }}>
                <EditableTable ref={friendsRef} />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 2, span: 20 }}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
};

export default Demo;
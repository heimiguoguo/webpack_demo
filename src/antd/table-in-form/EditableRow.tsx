
import React, { useContext, useEffect } from 'react'
import { Form } from 'antd';
import EditableContext, { FormListContext } from './EditableContext'

interface EditableRowProps {
    index: number;
}

const EditableRow: React.FC<EditableRowProps> = ({ index, ...props }) => {
    // console.log(index)
    // console.log(props)
    const rowKey = props['data-row-key']
    const [form] = Form.useForm();
    const { formMap } = useContext(FormListContext)

    useEffect(() => {
        // console.log('mounted', props)
        if (rowKey !== undefined) {
            formMap[rowKey] = form
        }
        return () => {
            // console.log('unmounted', props)
            if (rowKey !== undefined) {
                delete formMap[rowKey]
            }
        }
    }, [])

    return (
        <Form name={`friend-${rowKey}`} form={form} component={false}>
            <EditableContext.Provider value={form}>
                <tr {...props} />
            </EditableContext.Provider>
        </Form>
    );
};


export default EditableRow
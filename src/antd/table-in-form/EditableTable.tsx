import React, { useState, useEffect, forwardRef, useImperativeHandle } from 'react';
import { Table, Input, Button, Popconfirm, message, type FormInstance, Form, Select } from 'antd';
import EditableRow from './EditableRow'
import EditableCell from './EditableCell'
import { FormListContext } from './EditableContext'

const { Option } = Select

type EditableTableProps = Parameters<typeof Table>[0];

interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
}

type ColumnTypes = Exclude<EditableTableProps['columns'], undefined>;

const formMap = {}

const App: React.FC = forwardRef((props: EditableTableProps, ref) => {

    const [dataSource, setDataSource] = useState<DataType[]>([]);

    const [count, setCount] = useState(2);

    useEffect(() => {
        setDataSource([
            {
                key: '0',
                name: 'Edward King 0',
                age: 32,
                address: 'London, Park Lane no. 0',
            },
            {
                key: '1',
                name: 'Edward King 1',
                age: 32,
                address: 'London, Park Lane no. 1',
            },
        ])
        setCount(2)
    }, [])

    const handleDelete = (key: React.Key) => {
        const newData = dataSource.filter(item => item.key !== key);
        setDataSource(newData);
    };

    const handleAdd = () => {
        const newData: DataType = {
            key: count,
            name: '',
            age: null,
            address: '',
        };
        setDataSource([...dataSource, newData]);
        setCount(count + 1);
    };

    const handleSave = (row: DataType) => {
        const newData = [...dataSource];
        const index = newData.findIndex(item => row.key === item.key);
        const item = newData[index];
        newData.splice(index, 1, {
            ...item,
            ...row,
        });
        setDataSource(newData);
    };

    const validateFormList = async () => {
        if (Object.entries(formMap).length) {
            return await Promise.all(Object.values(formMap).map((form: FormInstance) => form.validateFields()))
        } else {
            return new Promise((resolve, reject) => {
                message.warn('请至少添加一个紧急联系人')
                reject('请至少添加一个紧急联系人')
            })
        }
    }

    useImperativeHandle(ref, () => {
        return {
            validateFormList
        }
    })

    const components = {
        body: {
            row: EditableRow,
            cell: EditableCell,
        },
    };

    const columns = [
        {
            title: 'name',
            dataIndex: 'name',
            width: '30%',
            FormItem: ({ disabled, save }) => {
                return <Form.Item name="name" rules={[{ required: true }]}>
                    <Input style={{ width: '100%' }} />
                </Form.Item>
            }
        },
        {
            title: 'age',
            dataIndex: 'age',
            FormItem: ({ disabled, save }) => {
                return <Form.Item name="age" rules={[{ required: true }]}>
                    <Select style={{ width: '100%' }} >
                        {
                            Array.from({ length: 99 }).map((item, index) => <Option key={index} label={index + 1}>{index + 1}</Option>)
                        }
                    </Select>
                </Form.Item>
            }
        },
        {
            title: 'address',
            dataIndex: 'address',
        },
        {
            title: 'operation',
            dataIndex: 'operation',
            render: (_, record: { key: React.Key }) =>
                dataSource.length >= 1 ? (
                    <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
                        <a>Delete</a>
                    </Popconfirm>
                ) : null,
        },
    ].map(col => {
        return {
            ...col,
            onCell: (record: DataType) => ({
                record,
                dataIndex: col.dataIndex,
                title: col.title,
                FormItem: col.FormItem,
                handleSave,
            }),
        };
    });

    return (
        <div>
            <Button onClick={handleAdd} type="primary" style={{ marginBottom: 16 }}>
                Add a row
            </Button>
            <FormListContext.Provider value={{ formMap }}>
                <Table
                    components={components}
                    rowClassName={() => 'editable-row'}
                    bordered
                    dataSource={dataSource}
                    columns={columns as ColumnTypes}
                />
            </FormListContext.Provider>
        </div>
    );
})

export default App;
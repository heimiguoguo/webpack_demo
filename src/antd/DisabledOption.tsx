import { Select } from 'antd';
import React from 'react';

const { Option } = Select;

const children: React.ReactNode[] = [];
for (let i = 10; i < 36; i++) {
    const disabled = i > 10 && i < 13
    children.push(
        <Option
            key={i.toString(36) + i}
            disabled={disabled}
        >
            {i.toString(36) + i}
        </Option>
    );
}

const handleChange = (value: string[]) => {
    console.log(`selected ${value}`);
};

const App: React.FC = () => (
    <>
        <Select
            mode="multiple"
            // 如果allowClear，那么disabled的options也可以被清掉
            // allowClear
            style={{ width: '100%' }}
            placeholder="Please select"
            defaultValue={['a10', 'c12']}
            onChange={handleChange}
        >
            {children}
        </Select>
    </>
);

export default App;
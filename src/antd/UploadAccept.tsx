import React, { useState } from 'react'
import { Upload, Button, Checkbox } from 'antd';


export const UploadAccept = () => {
    const [accept, setAccept] = useState([])
    return <div>
        <div>
            <Checkbox.Group options={['.PNG', '.JPG', '.WEBP']} value={accept} onChange={(checkedValue) => {
                console.log(checkedValue)
                setAccept(checkedValue)
            }}></Checkbox.Group>
        </div>
        <Upload accept={accept.join(',')}>
            <Button>Click to Upload</Button>
        </Upload>
    </div>
}
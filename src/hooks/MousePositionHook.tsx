import React, { useState, useEffect } from 'react'

// 参考 https://zhuanlan.zhihu.com/p/99048527
// 参考 https://blog.csdn.net/tonydz0523/article/details/106459906
const useMousePosition = () => {
    const [position, setPosition] = useState({ x: 0, y: 0 })
    useEffect(() => {
        const updateMouse = (e: MouseEvent) => {
            setPosition({ x: e.clientX, y: e.clientY })
        }
        document.addEventListener('mousemove', updateMouse)
        return () => {
            document.removeEventListener('mousemove', updateMouse)
        }
    })
    return position
}

export default useMousePosition
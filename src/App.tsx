import React, { Suspense, useState } from "react"
import {
    Routes,
    Route,
    Link,
    useNavigate
} from "react-router-dom"
import { IntlProvider } from 'react-intl'
import messages from './locales'
import { Layout, Menu, ConfigProvider, Tabs } from "antd"
import {
    ThemePicker,
    LocalePicker,
    localeList,
    CommonLocales
} from 'mot-ui'
import { routes, routerConfigType } from './router'
import 'antd/dist/antd.variable.min.css'
import './App.scss'


const { Sider, Content } = Layout;


export default function App() {
    const renderRoutes = (routes: routerConfigType[]) => {
        return routes.map((route, index) => {
            return <Route
                key={index}
                path={route.path}
                element={
                    // <KeepAlive name={route.path}>
                    <route.component></route.component>
                    // </KeepAlive>
                }
            >
                {route.routes && renderRoutes(route.routes)}
            </Route>
        })
    }
    const routesConfig = renderRoutes(routes)
    const [locale, setLocale] = useState(localeList[0])
    const defaultSelectedKeys = [`/${location.pathname.slice(1).split('/')[0]}`]
    // console.log(defaultSelectedKeys)
    // at(-1) 取数组最后一个元素(ES2022)
    const activeRoute = routes.find(route => route.path === location.pathname) || routes[0]
    const [activeKey, setActiveKey] = useState(activeRoute.path)
    const [tabList, setTabList] = useState([activeRoute])
    const navigate = useNavigate()

    const mergedMessages = {
        ...messages[locale.key],
        ...CommonLocales[locale.key],
    }

    const onChange = (activeKey: string) => {
        setActiveKey(activeKey)
    };

    const onEdit = (targetKey: string, action: 'add' | 'remove') => {
        // this[action](targetKey);
        if (action === 'add') {
            add(targetKey)
        } else {
            remove(targetKey)
        }
    };

    const add = (e) => {
        const activeKey = e.key
        setActiveKey(activeKey)
        if (!tabList.find(tab => tab.path === activeKey)) {
            setTabList([...tabList, routes.find(tab => tab.path === activeKey)])
        }
    };

    const remove = (targetKey: string) => {
        let newActiveKey: string;
        // 待删除的tab的前一个tab的index，可能为-1（当待删除tab为第一个tab时）
        const lastIndex: number = tabList.findIndex(tab => tab.path === targetKey) - 1
        const newTabList = tabList.filter(tab => tab.path !== targetKey);
        if (activeKey === targetKey) {
            if (lastIndex >= 0) {
                newActiveKey = newTabList[lastIndex].path;
            } else {
                newActiveKey = newTabList[0].path;
            }
            // 当待删除tab等于active tab的时候，需要重新计算新的active tab并跳转到对应路由
            setActiveKey(newActiveKey)
            navigate(newActiveKey)
        }
        setTabList(newTabList)
    };

    return (
        <IntlProvider locale={locale.key} messages={mergedMessages}>
            <ConfigProvider locale={locale.value}>
                <div className="app-container">
                    <div className="app-header">
                        <div className="app-logo"></div>
                        <div className="toolbox">
                            <ThemePicker />
                            <LocalePicker locale={locale} setLocale={setLocale} />
                        </div>
                    </div>

                    <Suspense fallback={<div>Loading...</div>}>
                        <Layout style={{ height: '100%' }}>
                            <Sider>
                                <Menu
                                    style={{ width: 200, height: '100%' }}
                                    defaultSelectedKeys={defaultSelectedKeys}
                                    mode="inline"
                                    // antd 版本 >= 4.22.0才支持items
                                    items={routes.map(route => {
                                        return {
                                            label: <Link to={route.path}>{route.name}</Link>,
                                            key: route.path
                                        }
                                    })}
                                    onClick={add}
                                />
                            </Sider>
                            <Content style={{ margin: 16, display: 'flex', flexDirection: 'column' }}>
                                <Tabs
                                    hideAdd
                                    onChange={onChange}
                                    activeKey={activeKey}
                                    type="editable-card"
                                    onEdit={onEdit}
                                    items={tabList.map(item => {
                                        return {
                                            key: item.path,
                                            label: <Link to={item.path}>{item.name}</Link>,
                                            // 当仅剩一个tab时不允许删除
                                            disabled: tabList.length === 1
                                        }
                                    })}
                                />
                                {/* <KeepAliveProvider> */}
                                <div className="app-content">
                                    <Routes>
                                        {routesConfig}
                                    </Routes>
                                </div>
                                {/* </KeepAliveProvider> */}
                            </Content>
                        </Layout>
                    </Suspense>
                </div>
            </ConfigProvider>
        </IntlProvider >
    )
}
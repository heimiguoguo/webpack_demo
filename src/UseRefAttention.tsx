import React, { useState, useMemo, useEffect, useRef } from 'react';

const App = () => {
	let [isChild, setChild] = useState(false);
	const ref = useRef(false)

	useEffect(() => {
		window.addEventListener('resize', onResize)
	}, [])

	const onResize = () => {
		console.log(isChild)
		console.log(ref.current)
	}

	return (
		<div>
			<Child isChild={isChild} name="child" />
			<button onClick={() => {
				setChild(!isChild)
				ref.current = !ref.current
			}}>改变Child</button>
		</div>
	);
}

let Child = (props) => {
	let getRichChild = () => {
		const result = props.isChild ? 'rich' : 'poor'
		console.log(result)
		return result
	}

	let richChild = useMemo(() => {
		//执行相应的函数
		return getRichChild();
	}, [props.isChild]);

	console.log('child rendering...')

	return (
		<div>
			isChild: {props.isChild ? 'true' : 'false'}<br />
			{richChild}
		</div>
	);
}

export default App
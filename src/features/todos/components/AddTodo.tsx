import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addTodo, setVisibilityFilter } from '../todoSlice'
import { Button, Form, Input } from 'antd'
import { useForm } from 'antd/lib/form/Form'


export default function AddTodo() {
  const dispatch = useDispatch()
  const filter = useSelector(state => state.todoApp.visibilityFilter)
  const [form] = useForm()

  const onSubmit = async ({ text }) => {
    dispatch(addTodo(text))
    form.resetFields()
    // 新增代办事项时，如果过滤器选择了Completed，自动切换到All，防止新增代办事项看不到，让人误以为是bug
    if (filter === 'SHOW_COMPLETED') {
      dispatch(setVisibilityFilter('SHOW_ALL'))
    }
  }

  return (
    <Form
      form={form}
      onFinish={onSubmit}
    >
      <Form.Item
        label=""
        name="text"
        rules={[{ required: true, message: '请输入代办事项' }]}
      >
        <Input className='todo-input' type="text" />
      </Form.Item>
      <Form.Item>
        <Button htmlType='submit' type='primary'>Add</Button>
      </Form.Item>
    </Form>
  );
}
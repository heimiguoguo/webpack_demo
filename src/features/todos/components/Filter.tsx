import React from 'react'
import { Radio } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { setVisibilityFilter } from '../todoSlice'

const Footer = () => {
  const visibilityFilter = useSelector((state: { todoApp: { visibilityFilter: string } }) => state.todoApp.visibilityFilter)
  const dispatch = useDispatch()
  const onChange = e => {
    const filter = e.target.value
    dispatch(setVisibilityFilter(filter))
  }
  return (
    <Radio.Group value={visibilityFilter} buttonStyle="solid" onChange={onChange}>
      <Radio.Button value='SHOW_ALL'>All</Radio.Button>
      <Radio.Button value='SHOW_ACTIVE'>Active</Radio.Button>
      <Radio.Button value='SHOW_COMPLETED'>Completed</Radio.Button>
    </Radio.Group>
  )
}

export default Footer
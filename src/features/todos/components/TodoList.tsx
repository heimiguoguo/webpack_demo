import React from 'react'
import { Button, Table, Tag } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useDispatch, useSelector } from 'react-redux'
import { selectTodo, toggleTodo } from '../todoSlice'

interface DataType {
  id: number;
  text: string;
  completed: boolean
}

const TodoList = () => {
  const todos = useSelector(selectTodo)
  const dispatch = useDispatch()

  const columns: ColumnsType<DataType> = [
    {
      title: '状态',
      key: 'completed',
      dataIndex: 'completed',
      render: (_, { completed }) => {
        let color = completed ? 'green' : 'volcano';
        return (
          <Tag color={color}>
            {completed ? '完成' : '未完成'}
          </Tag>
        );
      }
    },
    {
      title: '描述',
      dataIndex: 'text',
      key: 'text',
      render: text => <a>{text}</a>,
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Button size="small" type='primary' onClick={() => dispatch(toggleTodo(record.id))}>
          {record.completed ? '重做' : '完成'}
        </Button>
      ),
    },
  ];

  return (
    <Table rowKey='id' columns={columns} dataSource={todos} />
  )
}

export default TodoList
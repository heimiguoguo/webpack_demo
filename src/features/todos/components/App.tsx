import React, { useEffect } from 'react'
import Filter from './Filter'
import AddTodo from './AddTodo'
import VisibleTodoList from './TodoList'

const App = () => {
  return (
    <div>
      <AddTodo />
      <Filter />
      <VisibleTodoList />
    </div>
  )
}

export default App
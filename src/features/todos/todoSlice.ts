import { createSlice, nanoid } from '@reduxjs/toolkit'

export interface TodoProps {
    id: number,
    text: string,
    completed: boolean
}

let nextTodoId = 0

export const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todos: [],
        visibilityFilter: 'SHOW_ALL'
    },
    reducers: {
        addTodo(state, action) {
            state.todos.push({
                id: nextTodoId++,
                text: action.payload,
                completed: false
            })
        },
        // addTodo: {
        //     reducer(state, action) {
        //         state.todos.push(action.payload)
        //     },
        //     prepare(text: string) {
        //         return {
        //             payload: {
        //                 id: nanoid(),
        //                 text,
        //                 completed: false
        //             }
        //         }
        //     }
        // },
        toggleTodo: (state, action) => {
            const todo = state.todos.find(todo => todo.id === action.payload)
            todo.completed = !todo.completed
        },
        setVisibilityFilter: (state, action) => {
            state.visibilityFilter = action.payload
        }
    }
})

export const { addTodo, toggleTodo, setVisibilityFilter } = todoSlice.actions


// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectTodo = (state: {
    todoApp: {
        todos: TodoProps[],
        visibilityFilter: string
    }
}) => {
    const { todos, visibilityFilter } = state.todoApp
    switch (visibilityFilter) {
        case 'SHOW_ALL':
            return todos
        case 'SHOW_COMPLETED':
            return todos.filter(t => t.completed)
        case 'SHOW_ACTIVE':
            return todos.filter(t => !t.completed)
    }
}

export default todoSlice.reducer
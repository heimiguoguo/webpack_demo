import React, { useImperativeHandle, forwardRef, useEffect } from 'react'
import GrandSon from './GrandSon'

interface SonProps {
    label: string
    count: number
}


const Son: React.FC<SonProps> = forwardRef((props, ref) => {
    // Function components cannot be given refs. Attempts to access this ref will fail. Did you mean to use React.forwardRef()?
    let { label, count, ...rest } = props

    useEffect(() => {
        console.log('son mounted....')
    }, [])


    useImperativeHandle(
        ref,
        () => ({
        })
    )

    const handleChange = (e: any) => {
        count = count + 1
    }

    console.log('son rendering...')


    return <div className="son" style={{ border: '1px solid red', padding: 10 }}>
        <h4>Son的地盘</h4>
        <span>Count from paren: {count}</span>
        <GrandSon ref={null} {...rest}></GrandSon>
    </div>
})

export default Son
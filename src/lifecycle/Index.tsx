import React, { useRef, useState, useEffect } from 'react'
import { Row, Col, Button } from 'antd'
import Son from './Son'

const Parent = () => {
    const [count, setCount] = useState(0)
    const countRef = useRef(0)
    const childRef = useRef({ getValue: Function })

    useEffect(() => {
        console.log('parent mounted....')
    }, [])

    console.log('parent rendering...')

    return <div className="parent1234" style={{ border: '1px solid blue', padding: 10 }}>
        <h3>Parent的地盘</h3>

        <Row gutter={20}>
            <Col span={12}>
                Count:{count}
                <Button onClick={() => {
                    setCount(count + 1)
                }}>Add Count</Button>
                <Son ref={childRef} count={count} />
            </Col>
            <Col span={12}>
                {/* ref更新组件不会更新 */}
                Count:{countRef.current}
                <Button onClick={() => {
                    countRef.current = countRef.current + 1
                    console.log('countRef.current', countRef.current)
                }}>Add Count Ref</Button>
                <Son ref={childRef} count={countRef.current} />
            </Col>
        </Row>
    </div>
}

export default Parent
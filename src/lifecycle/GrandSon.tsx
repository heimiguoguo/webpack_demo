import React, { useEffect, ReactNode } from 'react'

interface GrandsonProps {
    grandsonRef: React.Ref<ReactNode>
}

const Grandson: React.FC<GrandsonProps> = ({ grandsonRef }) => {

    useEffect(() => {
        console.log('grandson mounted....')
    }, [])

    console.log('grandson rendering...')

    return <div style={{ border: '1px solid green', padding: 10 }}>
        <h5>Grandson的地盘</h5>
        <div className="hello">world</div>
    </div>
}

export default Grandson
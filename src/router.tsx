import { Component, lazy } from 'react'
import Locale from "./locale/Index"

export interface RouteType {
    index?: boolean,
    path: string,
    name: string,
    component: Component,
    routes?: RouteType[]
}

export interface routerConfigType {
    path: string,
    name?: string,
    component: Component,
    routes?: routerConfigType[]
}

export const routes = [
    {
        path: "/locale",
        name: "国际化",
        component: Locale
    },
    {
        path: "/todo-app",
        name: 'Todo App',
        component: lazy(() => import("./features/todos/components/App"))
    },
    {
        path: "/form-render",
        name: "表单编辑器",
        component: lazy(() => import("./form-render"))
    },
    {
        path: "/react-draggable-tags",
        name: "流溪(基于react-draggable-tags实现)",
        component: lazy(() => import("./hoc/react-draggable-tags/LiuXi"))
    },
    {
        path: "/antd-demo",
        name: "Antd",
        component: lazy(() => import("./antd/CustomAnchor")),
        // routes: [
        //     {
        //         path: "registration-form",
        //         name: 'Registration Form',
        //         component: lazy(() => import("./antd/RegistrationForm")),
        //     },
        //     {
        //         path: "table-in-form",
        //         name: 'Table in Form',
        //         component: lazy(() => import("./antd/table-in-form"))
        //     },
        //     {
        //         path: "custom-anchor",
        //         name: 'Custom Anchor',
        //         component: lazy(() => import("./antd/CustomAnchor"))
        //     },
        //     {
        //         path: "disabled-option",
        //         name: 'Disabled Option',
        //         component: lazy(() => import("./antd/DisabledOption"))
        //     },
        //     {
        //         path: "searchable-select",
        //         name: 'Searchable Select',
        //         component: lazy(() => import("./antd/SearchableSelect"))
        //     },
        // ]
    },
    // {
    //     path: "/react-pdf",
    //     name: "React PDF",
    //     component: lazy(() => import("./pdf/Index"))
    // },
    // {
    //     path: "/react-lifecycle",
    //     name: "React组件生命周期",
    //     component: lazy(() => import("./lifecycle/Index"))
    // },
    {
        path: "/grid-layout",
        name: "网格布局",
        component: lazy(() => import("./grid-layout"))
    },
]
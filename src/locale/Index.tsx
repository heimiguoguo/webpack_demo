import React from 'react'
import { FormattedMessage, injectIntl, useIntl } from 'react-intl'
import { DatePicker, TimePicker, Table, Divider, Input } from 'antd'
import { IntlApp } from 'mot-ui'
// import IntlApp from '../../../mot-ui/src/components/SelfIntl'

const dataSource = [
    {
        key: '1',
        name: <FormattedMessage id='user.name' />,
        age: 32,
        address: <FormattedMessage id='user.address' />,
    }
];

// const InjectedIntlApp = injectIntl(IntlApp)

export default function () {

    const intl = useIntl()

    const columns = [
        {
            title: <FormattedMessage id='column.name' />,
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: <FormattedMessage id='column.age' />,
            dataIndex: 'age',
            key: 'age',
        },
        {
            title: <FormattedMessage id='column.address' />,
            dataIndex: 'address',
            key: 'address',
        },
    ];

    return (
        <div>
            <Input
                // placeholder={<FormattedMessage id='column.age' />}
                placeholder={intl.formatMessage({
                    id: 'placeholder'
                })}
            />
            <br></br><br></br>

            <DatePicker />
            <TimePicker />
            <br></br><br></br>

            <Table dataSource={dataSource} columns={columns} />

            <Divider></Divider>
            <IntlApp />
        </div>
    )
}
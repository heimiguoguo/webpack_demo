import React from 'react';
import style from './Index.module.less'


export default () => {
    return (
        <div className={style.parent}>
            {
                Array.from({ length: 9 }).map((item, index) => {
                    return <div
                        key={index}
                        className={style.child}>{index + 1}</div>
                })
            }
            <div className={style.child}>附件</div>
        </div>
    );
};
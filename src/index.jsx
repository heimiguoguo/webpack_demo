import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import App from './App'
import { BrowserRouter as Router } from 'react-router-dom'

const root = document.createElement('div')
root.id = 'root'
document.body.appendChild(root)

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>,
    document.getElementById('root')
)


// if ('serviceWorker' in navigator) {
//     window.addEventListener('load', () => {
//         navigator.serviceWorker.register('./service-worker.js').then(registration => {
//             console.log('SW registered: ', registration)
//         }).catch(registrationError => {
//             console.log('SW registration failed: ', registrationError)
//         })
//     })
// }
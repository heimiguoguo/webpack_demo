const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// 显示进程的完成进度
const ProgressBarPlugin = require('progress-bar-webpack-plugin')
// 设置进度字体颜色
const chalk = require('chalk')
// 如果预先定义过环境变量，就将其赋值给`ASSET_PATH`变量，否则赋值为根目录
const ASSET_PATH = process.env.ASSET_PATH || '/'

module.exports = {
    entry: {
        app: './src/index.js',
    },
    resolve: {
        alias: {
            '@src': path.resolve(__dirname, './src')
        },
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
        // @react-pdf/renderer相关配置
        fallback: {
            process: require.resolve("process/browser"),
            zlib: require.resolve("browserify-zlib"),
            stream: require.resolve("stream-browserify"),
            util: require.resolve("util"),
            buffer: require.resolve("buffer"),
            asset: require.resolve("assert"),
        }
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'style-loader'
                    },
                    {
                        loader: 'css-loader'
                    },
                    {
                        loader: 'postcss-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: "[local]___[hash:base64:5]"
                            }
                        },
                    },
                    {
                        loader: 'less-loader',
                        options: {
                            javascriptEnabled: true
                        },
                    }
                ]
            },
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {
                test: /\.tsx?$/,
                // include: path.resolve(__dirname, 'src'),
                loader: "ts-loader",
                // 以下配置使项目启动时间从50多s直接降到27s
                options: {
                    transpileOnly: true,
                },
            },

            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(png|svg|jpg|gif|jpeg)$/,
                use: [
                    'file-loader'
                ]
            },
            // {
            //     test: /\.svg$/,
            //     use: [
            //         {
            //             loader: "babel-loader"
            //         },
            //         {
            //             loader: "react-svg-loader",
            //             options: {
            //                 jsx: true // true outputs JSX tags
            //             }
            //         }
            //     ]
            // }
        ]
    },
    plugins: [
        require('autoprefixer'),
        new ProgressBarPlugin({
            format: '  build [:bar] ' + chalk.green.bold(':percent') + ' (:elapsed seconds)',
            clear: false
        }),
        new HtmlWebpackPlugin({
            title: 'Hello React',
            template: 'public/index.html'
        }),
        new webpack.ProvidePlugin({
            join: ['lodash', 'join'],
            // @react-pdf/renderer相关配置
            Buffer: ["buffer", "Buffer"],
            process: "process/browser",
        }),
        // 该插件帮助我们安心地使用环境变量
        new webpack.DefinePlugin({
            'process.env.ASSET_PATH': JSON.stringify(ASSET_PATH)
        }),
    ],
    optimization: {
        splitChunks: {
            chunks: "all",
            name: false,
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/](react|react-dom|react-router-dom|moment|antd|react-intl)/,
                    name: "vendors",
                    chunks: "all"
                }
            }
        }
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: ASSET_PATH,
        // https://webpack.docschina.org/guides/build-performance/#progress-plugin
        // 输出结果不携带路径信息
        pathinfo: false
    },
}
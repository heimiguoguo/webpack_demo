// vite.config.js
import { defineConfig } from 'vite';
import less from 'less';
 
export default defineConfig({
  plugins: [
    // ...
  ],
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
});